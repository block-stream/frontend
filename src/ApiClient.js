import axios from 'axios';

const client = axios.create({
  baseURL: '//api.blockstream.crucify.io/api',
  withCredentials: true
});

export default client;
