import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Divider, Drawer } from '@material-ui/core';
import LockIcon from '@material-ui/icons/Lock';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ApiClient from '../../../../ApiClient';

import { Profile, SidebarNav } from './components';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, ...rest } = props;

  const classes = useStyles();

  const pages = [
    /*
    {
      title: 'Dashboard',
      href: '/dashboard',
      icon: <DashboardIcon />
    },
    {
      title: 'Account',
      href: '/account',
      icon: <AccountBoxIcon />
    },
    {
      title: 'Settings',
      href: '/settings',
      icon: <SettingsIcon />
    },
    */
    {
      title: 'Popular Movies',
      href: '/popular-movies',
      icon: <ArrowForwardIosIcon />
    },
    {
      title: 'Hindi Movies',
      href: '/hindi-movies',
      icon: <ArrowForwardIosIcon />
    },
    {
      title: 'English Movies',
      href: '/english-movies',
      icon: <ArrowForwardIosIcon />
    }
  ];

  const loginPages = [
    {
      title: 'Sign In',
      href: '/sign-in',
      icon: <LockIcon />
    },
    {
      title: 'Sign Up',
      href: '/sign-up',
      icon: <PersonAddIcon />
    }
  ];

  let [isLoggedIn, setIsLoggedIn] = useState(false);
  const [selfData, setSelfData] = useState({});

  useEffect(() => {
    ApiClient.get('/').then(res => {
      setSelfData(res.data);
      res.data.isLoggedIn ? setIsLoggedIn(true) : setIsLoggedIn(false);
    });
  }, [])

  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}>
      <div {...rest} className={clsx(classes.root, className)}>
        {isLoggedIn ? (
          <Profile userData={selfData}/>
        ) : (
          <SidebarNav
            className={classes.nav}
            pages={loginPages}
            style={{ marginBottom: 0 }}
          />
        )}
        <Divider className={classes.divider} />
        <SidebarNav className={classes.nav} pages={pages} />
      </div>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;
