import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

import ApiClient from '../../../../../../ApiClient';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 60,
    height: 60
  },
  name: {
    marginTop: theme.spacing(1)
  }
}));

const Profile = props => {
  const { userData, className, ...rest } = props;

  const classes = useStyles();

  const [user, setUser] = useState({
    name: '',
    avatar: '/images/avatars/avatar_11.png'
  });

  useEffect(() => {
    console.log('calling');
    ApiClient.get('/').then(res => {
      console.log(res.data);
      setUser({
        ...userData,
        name: `${res.data.first_name} ${res.data.last_name}`
      });
      // setUser({ ...user, name: `Shen Zhi` });
    });
  }, [userData]);

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Avatar
        alt="Person"
        className={classes.avatar}
        component={RouterLink}
        to="/settings"
      >
        {user.name
          .split(/\s+/g)
          .map(a => a.length > 0 ? a[0].toUpperCase() : '')
          .join('')}
      </Avatar>
      <Typography
        className={classes.name}
        variant="h4"
      >
        {user.name}
      </Typography>
      <Typography variant="body2">{user.bio}</Typography>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
