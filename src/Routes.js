import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Dashboard as DashboardView,
  Account as AccountView,
  Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView,
  PopularMovies as PopularMoviesView,
  EnglishMovies as EnglishMoviesView,
  HindiMovies as HindiMoviesView,
  WatchingMovies as WatchingMoviesView
} from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/popular-movies"
      />
      <Redirect
        exact
        from="/hindi-movies"
        to="/hindi-movies/1"
      />
      <Redirect
        exact
        from="/english-movies"
        to="/english-movies/1"
      />
      <Redirect
        exact
        from="/popular-movies"
        to="/popular-movies/1"
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        path="/dashboard"
      />
      <RouteWithLayout
        component={AccountView}
        exactgt
        layout={MainLayout}
        path="/account"
      />
      <RouteWithLayout
        component={SettingsView}
        exact
        layout={MainLayout}
        path="/settings"
      />
      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path="/sign-up"
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />
      <RouteWithLayout
        component={PopularMoviesView}
        exact
        layout={MainLayout}
        path="/popular-movies/:page"
      />
      <RouteWithLayout
        component={HindiMoviesView}
        exact
        layout={MainLayout}
        path="/hindi-movies/:page"
      />
      <RouteWithLayout
        component={EnglishMoviesView}
        exact
        layout={MainLayout}
        path="/english-movies/:page"
      />
      <RouteWithLayout
        component={WatchingMoviesView}
        exact
        layout={MainLayout}
        path="/movies/:movieId"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
