import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {
  Redirect,
  useParams,
  useHistory,
  useRouteMatch
} from 'react-router-dom';
import Pagination from 'material-ui-flat-pagination';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    width: '100%'
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)'
  },
  pagination: {
    rootCurrent: {
      color: 'red'
    }
  }
}));

export default function ListOfMovies(props) {
  const classes = useStyles();

  const { movies, paginated } = props;
  console.log(movies);
  const [redirect, setRedirect] = useState();
  const { page } = useParams();
  const [currentPage, setCurrentPage] = useState(page || 1);
  console.log(currentPage);

  const perPage = paginated ? 40 : Infinity;
  const totalPages = Math.ceil(movies.length / perPage);
  const history = useHistory();
  const match = useRouteMatch();

  useEffect(() => {
    if (paginated) {
      history.push(
        [...match.path.split('/').slice(0, -1), currentPage].join('/')
      );
    }
  }, [currentPage]);

  return (
    <div
      className={classes.root}
      style={{ display: 'flex', flexDirection: 'row' }}>
      <div>
        {movies
          .slice((currentPage - 1) * perPage, currentPage * perPage)
          .map((tile, i) => (
            <div
              key={i}
              onClick={() =>
                setRedirect(<Redirect push to={`/movies/${tile.movie_id || tile._id}`} />)
              }
              style={{
                cursor: 'pointer',
                display: 'inline-flex',
                flexDirection: 'column',
                margin: 5,
                position: 'relative'
              }}>
              <img
                alt={tile.name}
                src={tile.poster || ""}
                style={{ padding: 0, margin: 0, width: 185, minHeight: 270 }}
              />

              <div
                style={{
                  backgroundColor: 'rgba(0, 0, 0, 0.7)',
                  height: '3em',
                  padding: 0,
                  margin: 0,
                  position: 'absolute',
                  bottom: 0,
                  color: 'white',
                  boxSizing: 'border-box',
                  display: 'flex',
                  flexDirection: 'column'
                }}>
                <span
                  style={{
                    margin: 'auto',
                    textAlign: 'center',
                    width: 185
                  }}>
                  {tile.name}
                </span>
              </div>
            </div>
          ))}
      </div>

      <Pagination
        className={classes.pagination}
        currentPageColor="secondary"
        limit={1}
        offset={currentPage - 1}
        onClick={(e, offset) => {
          console.log(offset);
          setCurrentPage(offset + 1);
        }}
        otherPageColor="secondary"
        style={{ margin: 'auto' }}
        total={totalPages}
      />

      {redirect}
    </div>
  );
}
