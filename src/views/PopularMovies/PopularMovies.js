import { Movies } from '../';
import { withRouter } from 'react-router-dom';
import React from 'react';

const PopularMovies = props => {
return <Movies movies="popular" />;
};

export default withRouter(PopularMovies);
