import { ListOfMovies } from '../../components';
import React, { useState, useEffect } from 'react';
import ApiClient from '../../ApiClient';

export default function Movies(props) {
  const filter = props.movies;
  console.log(filter);

  const [data, setData] = useState([]);

  useEffect(() => {
    let endpoint = null;
    switch (filter) {
      case 'popular':
        endpoint = '/movies/popular';
        break;
      case 'hindi':
        endpoint = '/movies/hindi';
        break;
      case 'english':
        endpoint = '/movies/western';
        break;
    }

    console.log('endpoint')
    ApiClient.get(endpoint).then(res => setData(res.data));
  }, []);

  // const movies = getMovies(filter);
  return <ListOfMovies movies={data} paginated={true}/>;
}
