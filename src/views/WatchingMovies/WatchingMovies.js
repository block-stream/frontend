import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ApiClient from '../../ApiClient';
import VideoCamIcon from '@material-ui/icons/Videocam';

export default function WatchingMovies(props) {
  const { movieId } = useParams();
  const [data, setData] = useState({
    directors: [],
    actors: [],
    productions: []
  });

  useEffect(() => {
    ApiClient.get(`/movies/${movieId}`).then(res => setData(res.data));
  }, []);

  useEffect(() => console.log(data), [data]);

  return (
    <div
      style={{
        display: 'flex',
        flexFlow: 'column',
        height: '100%',
      }}
    >
      <video
        controls
        style={{ minHeight: 0 }}
      >
        <source
          src="http://download.blender.org/peach/trailer/trailer_1080p.mov"
          type="video/quicktime"
        />
        <source
          src="mov_bbb.ogg"
          type="video/ogg"
        />
        Your browser does not support HTML5 video.
      </video>
      <div>
        <img
          src={data.poster}
          style={{ float: 'left' }}
        />
        <h1>{data.name}</h1>
        <hr />
        <p>Directors: {data.directors.join(', ')}</p>
        <hr />
        <p>Actors: {data.actors.join(', ')}</p>
        <hr />
        <p>Productions: {data.productions.join(', ')}</p>
        <hr />
        <p>{data.description}</p>
      </div>
    </div>
  );
}
