import { Movies } from '../';
import { withRouter } from 'react-router-dom';
import React from 'react';

const EnglishMovies = props => {
  return <Movies movies="english" />;
};

export default withRouter(EnglishMovies);
