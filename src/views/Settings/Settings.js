import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Divider from '@material-ui/core/Divider';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Dropzone from 'react-dropzone';
import { Notifications, Password } from './components';
import ApiClient from '../../ApiClient';
import ChipInput from 'material-ui-chip-input';
import MovieIcon from '@material-ui/icons/Movie';
import validate from 'validate.js';
import { ListOfMovies } from '../../components';

import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import LinearProgress from '@material-ui/core/LinearProgress';
import InputAdornment from '@material-ui/core/InputAdornment';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const EditUserDetails = props => {
  const [user, setUser] = useState({})

  useEffect(() => {
    ApiClient.get('/')
      .then(res => res.data)
      .then(data => setUser(data));
  }, [])

  return (
    <Card>
      <CardHeader subheader="Manage your details" title="Profile" />
      <Divider />
      <div>
      </div>
    </Card>
  );
};

const SelfMovies = props => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    ApiClient.get('/movies/self')
      .then(res => res.data)
      .then(data => setMovies(data))
      .catch(err => console.error(err));
  }, []);

  return (
    <div>
      Your Movies
      <ListOfMovies movies={movies} paginated={false} />
    </div>
  );
};

const VideoUploadDialog = props => {
  const { open, onDialogClose, video } = props;
  const [filename, setFilename] = useState('');
  const [form, setForm] = useState({ language: 'english' });
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    setFilename(video !== undefined ? video.name : '');
  }, [video]);

  const handleClose = data => {
    onDialogClose(data);
  };

  const submitData = () => {
    const formData = new FormData();
    Object.keys(form).map(key => formData.set(key, form[key]));
    formData.set('video', video, video.name);

    ApiClient.post('/upload', formData, {
      onUploadProgress: progressEvent =>
        setProgress(
          Math.round((progressEvent.loaded * 100) / progressEvent.total)
        ),
      headers: { 'Content-Type': 'multipart/form-data' }
    })
      .then(res => handleClose({ success: true }))
      .catch(err => handleClose({ error: true }));
  };

  const handleChange = event => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  const handleChipChange = (name, chips) => {
    setForm({ ...form, [name]: chips });
  };

  return (
    <div>
      <Dialog
        aria-labelledby="form-dialog-title"
        onClose={() => handleClose({ cancel: true })}
        open={open}>
        <DialogTitle id="form-dialog-title">Upload Movie</DialogTitle>
        <DialogContent>
          <div
            style={{
              backgroundColor: 'lightgrey',
              paddingTop: '25px',
              paddingBottom: '25px',
              textAlign: 'center',
              display: 'flex'
            }}>
            <p style={{ margin: 'auto' }}>
              <MovieIcon />
              <br />
              {filename}
            </p>
          </div>

          <TextField
            autoFocus
            fullWidth
            label="Name"
            margin="dense"
            name="name"
            onChange={handleChange}
            type="email"
          />
          <ChipInput
            fullWidth
            label="Directors"
            name="directors"
            onChange={chips => handleChipChange('directors', chips)}
          />
          <br />
          <ChipInput
            fullWidth
            label="Actors"
            name="actors"
            onChange={chips => handleChipChange('actors', chips)}
          />
          <br />
          <ChipInput
            fullWidth
            label="Productions"
            name="productions"
            onChange={chips => handleChipChange('productions', chips)}
          />
          <TextField
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">$</InputAdornment>
              )
            }}
            label="Price"
            name="price"
            onChange={handleChange}
            type="number"
          />
          <RadioGroup
            aria-label="language"
            defaultValue="english"
            name="language"
            onChange={handleChange}
            style={{ flexDirection: 'row' }}>
            <FormControlLabel
              control={<Radio />}
              label="English"
              value="english"
            />
            <FormControlLabel control={<Radio />} label="Hindi" value="hindi" />
          </RadioGroup>
          <TextField
            fullWidth
            label="Description"
            margin="dense"
            name="description"
            onChange={handleChange}
            type="email"
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={() => handleClose({ cancel: true })}>
            Cancel
          </Button>
          <Button color="primary" onClick={submitData}>
            Upload
          </Button>
        </DialogActions>
        <LinearProgress value={progress} variant="determinate" />
      </Dialog>
    </div>
  );
};

const UploadVideoDropZone = props => {
  const [overUpload, setOverUpload] = useState(false);
  const [dragActive, setDragActive] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [file, setFile] = useState({ name: '' });

  const classes = useStyles();

  const [snackBarState, setSnackBarState] = useState({
    open: false,
    vertical: 'bottom',
    horizontal: 'center',
    error: false,
    message: ''
  });

  const onSnackBarClose = () => {
    setSnackBarState({ ...snackBarState, open: false });
  };

  return (
    <div>
      <Dropzone
        accept="video/*"
        multiple={false}
        onDragEnter={() => setDragActive(true)}
        onDragLeave={() => setDragActive(false)}
        onDrop={acceptedFiles => {
          setDragActive(false);
          setDialogOpen(true);
          setFile(acceptedFiles[0]);
        }}
        onDropAccepted={() => {}}
        onDropRejected={() =>
          setSnackBarState({
            ...snackBarState,
            open: true,
            severity: 'error',
            message: 'Please select a video file'
          })
        }>
        {({ getRootProps, getInputProps }) => (
          <div id="upload-card" {...getRootProps()}>
            <Card
              className={classes.card}
              onMouseEnter={() => setOverUpload(true)}
              onMouseLeave={() => setOverUpload(false)}
              style={{
                cursor: 'pointer',
                backgroundColor:
                  overUpload || dragActive ? 'grey' : 'lightgrey',
                display: 'flex',
                justifyContent: 'center',
                height: '50vh',
                flexDirection: 'column'
              }}>
              <input {...getInputProps()} />
              <p style={{ margin: 'auto', textAlign: 'center' }}>
                <CloudUploadIcon style={{ fontSize: '10vh' }} />
                <br />
                Drag 'n' drop some files here, or click to select files
              </p>
            </Card>
          </div>
        )}
      </Dropzone>
      <Snackbar
        anchorOrigin={{
          vertical: snackBarState.vertical,
          horizontal: snackBarState.horizontal
        }}
        autoHideDuration={6000}
        key={`${snackBarState.vertical},${snackBarState.horizontal}`}
        onClose={onSnackBarClose}
        open={snackBarState.open}>
        <Alert onClose={onSnackBarClose} severity={snackBarState.severity}>
          {snackBarState.message}
        </Alert>
      </Snackbar>
      <VideoUploadDialog
        onDialogClose={({ success, error, cancel }) => {
          if (success) {
            setSnackBarState({
              ...snackBarState,
              open: true,
              severity: 'success',
              message: 'Video uploaded successfully!'
            });
          } else if (error) {
            setSnackBarState({
              ...snackBarState,
              open: true,
              severity: 'error',
              message: 'An error occurred while uploading, try again!'
            });
          } else if (cancel) {
            setSnackBarState({
              ...snackBarState,
              open: true,
              severity: 'info',
              message: 'Upload cancelled.'
            });
          }
          setDialogOpen(false);
        }}
        open={dialogOpen}
        video={file}
      />
    </div>
  );
};

const Settings = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item md={7} xs={12}>
          <EditUserDetails />
        </Grid>
        <Grid item md={5} xs={12}>
          <UploadVideoDropZone />
        </Grid>
      </Grid>
      <Grid item md={12} xs={12}>
        <SelfMovies />
      </Grid>
    </div>
  );
};

export default Settings;
