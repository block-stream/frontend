import { Movies } from '../';
import { withRouter } from 'react-router-dom';
import React from 'react';

const HindiMovies = props => {
  return <Movies movies="hindi"/>;
};

export default withRouter(HindiMovies);
